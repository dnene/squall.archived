package org.kotyle.squall.util

import org.slf4j.LoggerFactory
import java.sql.Statement

val log = LoggerFactory.getLogger("org.kotyle.squall.util")

class SquallSchemaException(val msg: String, val args: Map<String,Any?>, override val cause: Throwable? = null): Exception()
class SquallRuntimeException(val msg: String, val args: Map<String,Any?> = mapOf(), override val cause: Throwable? = null): Exception()

fun Statement.executeX(sql: String): Boolean {
    log.debug("Executing SQL: ${sql}")
    return this.execute(sql)
}