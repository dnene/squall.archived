package org.kotyle.squall.schema.tables

import java.util.ArrayList

abstract class Expression<out T>() {
}

abstract class ColumnTypeExpression<T> : Expression<T>() {
    abstract val columnType: ColumnType;
}

abstract class ColumnType(val nullable: Boolean = false, val autoinc: Boolean = false, val default: Any?) {
    abstract val javaSqlType: Int
}

open class Column<T: Any>(val table: Table, val name: String, override val columnType: ColumnType, val priority: Int, val pkIndex: Int? = null): ColumnTypeExpression<T>() {
    override fun toString() = "Column(${name}:${columnType}:${table._name})"
    fun defaultValue(): T? = columnType.default?.let { it as T? }
}

abstract class View() {
    abstract val columns: List<Column<*>>
}


open class Table(name: String?=null): View() {
    protected val _columns = ArrayList<Column<*>>()
    override val columns: List<Column<*>> = _columns
    open val _defaultValues by lazy {
        _columns.map { it to it.defaultValue()}.filter { it.second != null }.toMap()
    }
    val columnNameList by lazy { _columns.map { it.name }.joinToString(", ") }
    val _name = name ?: javaClass.simpleName.let {
        val lastIndex = it.lastIndexOf("Table")
        val strLen = it.length
        if (lastIndex >= 0 && (lastIndex + 5 == strLen)) {
            it.substring(0, lastIndex)
        } else it
    }
    init {
        println("Table name is ${_name}")
    }

    private fun <T: Any> registerColumn(name: String, priority: Int, type: ColumnType, pkIndex: Int? = null): Column<T> =
            Column<T>(this, name, type, priority, pkIndex).apply {
                val insertAtIndex = _columns.withIndex().fold((null as Int?)) { acc, (index, elem) ->
                    if (priority >= elem.priority) index else acc
                }.let { if (it == null) 0 else (it + 1) }
                _columns.add(insertAtIndex, this)
            }


    private fun <T: Any> registerIDColumn(name: String, priority: Int, type: ColumnType, pkIndex: Int? = null, idColumn: Boolean = false): Column<ID<T>> =
            Column<ID<T>>(this, name, type, priority, pkIndex).apply {
                val insertAtIndex = _columns.withIndex().fold((null as Int?)) { acc, (index, elem) ->
                    if (priority >= elem.priority) index else acc
                }.let { if (it == null) 0 else (it + 1) }
                _columns.add(insertAtIndex, this)
            }


    @Suppress("UNCHECKED_CAST")
    fun <T:Any> Column<T>.entityId(): Column<ID<T>> {
        val idCol = Column<ID<T>>(this.table, this.name, this.columnType, this.priority, this.pkIndex)
//        this.indexInPK = this@entityId.indexInPK
//        this.defaultValueFun = this@entityId.defaultValueFun?.let { { ID(it(), table as IdTable<T>) } }
        return idCol
    }

    fun long(name: String, nullable: Boolean =false, autoinc: Boolean=false, pkIndex: Int? = null, priority: Int = 100, default: Long? = null): Column<Long> = registerColumn(name, priority, LongColumnType(nullable = nullable, autoinc=autoinc, default=default), pkIndex = pkIndex)
    fun longid(name: String, nullable: Boolean =false, autoinc: Boolean=false, pkIndex: Int? = null, priority: Int = 100, default: Long? = null): Column<ID<Long>> = registerIDColumn(name, priority, LongColumnType(nullable = nullable, autoinc=autoinc, default=default), pkIndex = pkIndex)
    fun int(name: String, nullable: Boolean =false, autoinc: Boolean=false, pkIndex: Int? = null, priority: Int = 100, default: Int? = null): Column<Int> = registerColumn(name, priority, IntColumnType(nullable = nullable, autoinc=autoinc, default=default), pkIndex = pkIndex)
    fun varchar(name: String, maxlen: Int, nullable: Boolean =false, pkIndex: Int? = null, priority: Int = 100, default: String? = null): Column<String> = registerColumn(name, priority, VarcharColumnType(maxlen, nullable = nullable, default=default), pkIndex = pkIndex)
}


