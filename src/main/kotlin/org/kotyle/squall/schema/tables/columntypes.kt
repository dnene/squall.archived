package org.kotyle.squall.schema.tables

import java.sql.Types

class LongColumnType(nullable: Boolean, autoinc: Boolean = true, default: Long?): ColumnType(nullable = nullable, autoinc = autoinc, default=default) {
    override val javaSqlType = java.sql.Types.BIGINT
}

class IntColumnType(nullable: Boolean, autoinc: Boolean = true, default: Int?): ColumnType(autoinc = autoinc, default=default) {
    override val javaSqlType = java.sql.Types.INTEGER
}

abstract class ConstrainedColumnType(val maxlen: Int, nullable: Boolean = false, default: String?): ColumnType(nullable, autoinc = false, default=default) {

}
class VarcharColumnType(maxlen: Int, nullable: Boolean, default: String?): ConstrainedColumnType(maxlen, nullable = nullable, default=default) {
    override val javaSqlType = java.sql.Types.VARCHAR
}
