package org.kotyle.squall.schema.tables

class ID<T: Any>(val id: T?) {
    override fun toString() = "ID(${id})"
}

interface Identifiable<T: Any> {
    val id: Column<ID<T>>
}

abstract class IdTable<T:Any>(name: String?=null): Table(name), Identifiable<T> {

}

abstract class LongIdTable(name: String? = null, columnName: String = "id") : IdTable<Long>(name) {
    override val id: Column<ID<Long>> = longid(columnName, autoinc = true, pkIndex = 0, priority=10)
}

interface Versionable {
    val version: Column<Int>
}

open class LongIdVersionableTable(name: String? =null, idColumnName: String = "id", versionColumnName: String="version"):
        Table(name), Identifiable<Long>, Versionable {
    override val id: Column<ID<Long>> = longid(idColumnName, autoinc = true, pkIndex = 0, priority=10)
    override val version: Column<Int> = int(versionColumnName, autoinc = false, pkIndex = 0, priority=999, default=1)
}