package org.kotyle.squall.schema.tables.commands

import org.kotyle.squall.runtime.session.StatementType
import org.kotyle.squall.runtime.session.Session
import org.kotyle.squall.runtime.session.toSequence
import org.kotyle.squall.schema.tables.Column
import org.kotyle.squall.schema.tables.ID
import org.kotyle.squall.schema.tables.Table
import org.kotyle.squall.util.SquallRuntimeException
import org.kotyle.squall.util.executeX
import org.slf4j.LoggerFactory
import java.sql.ResultSet

val log = LoggerFactory.getLogger("org.kotyle.squall.schema.tables.statements")
interface SqlStatement {
    val table: Table
    val statementType: StatementType
    fun execute(session: Session)
}

open class RecordData<T: Table>(val table: T, initialiser: (RecordData<T>.() -> Unit)?=null) {
    val data = mutableMapOf<Column<*>, Any?>()
    init {
        if (initialiser != null) initialiser()
    }

    operator fun <S: Any> set(column: Column<S>, value: S?) {
        if (data.containsKey(column)) {
            throw SquallRuntimeException("err-column-already-initialised-in-insert", mapOf("column-name" to column.name, "table-name" to table._name))
        }
        if (!column.columnType.nullable && value == null) {
            throw SquallRuntimeException("err-column-does-not-allow-null-values", mapOf("column-name" to column.name, "table-name" to table._name))
        }
        data[column] = value
    }
}

sealed class BaseStatement<T: Table>(table: T, override val statementType: StatementType, initialiser: (RecordData<T>.() -> Unit)?=null): RecordData<T>(table, initialiser), SqlStatement {
}

class RefID<T: Table, R>(table: T) {
    var id: R? = null

    fun setIdentifer(any: Any) {
        (any as? R)?.let { id = it }
    }
}

open class InsertStatement<T: Table>(table: T, initialiser: (RecordData<T>.() -> Unit)?=null): BaseStatement<T>(table, StatementType.INSERT, initialiser) {
    override fun execute(session: Session) {
        val toInsert = table.columns.map {
            (data[it] ?: table._defaultValues[it]).let { value -> (value as? ID<*>)?.let { it.id } ?: value}
        }
        log.debug("Inserting data: ${table._name} ${table.columnNameList} ${toInsert}")
        log.debug("Insert statement: ${session.statementGenerator.generateInsertPreparedStatementFor(table)} with values ${toInsert}")
        val preparedStatement = session.getPreparedStatement(table, StatementType.INSERT)
        toInsert.forEachIndexed { index, value -> preparedStatement.setObject(index+1, value) }
        preparedStatement.addBatch()
    }
}

class InsertIDStatement<T: Table, R: Number>(table: T, initialiser: (RecordData<T>.() -> Unit)?=null): InsertStatement<T>(table, initialiser) {
    val generatedID = RefID<T,R>(table)
}

class DeleteStatement<T: Table>(table: T): BaseStatement<T>(table, StatementType.DELETE, null) {
    override fun execute(session: Session) {
        val preparedStatement = session.getPreparedStatement(table, StatementType.DELETE)
        preparedStatement.addBatch()
    }
}

class SelectStatement<T: Table>(table: T): BaseStatement<T>(table, StatementType.SELECT, null) {
    override fun execute(session: Session) {
        val preparedStatement = session.getPreparedStatement(table, StatementType.SELECT)
        val rs: ResultSet = preparedStatement.executeQuery()

        val records = mutableListOf<RecordData<T>>()
        while(rs.next()) {
            val record = RecordData<T>(table)
            for (index in 1..table.columns.size) {
                record.set<Any>(table.columns[index-1] as Column<Any>, rs.getObject(index) as Any)
            }
            records.add(record)
        }
    }
}