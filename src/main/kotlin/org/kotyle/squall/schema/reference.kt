//package org.kotyle.squall.schema
//
//import java.lang.IllegalArgumentException
//import java.lang.UnsupportedOperationException
//import java.sql.Connection
//import java.sql.DatabaseMetaData
//import java.sql.ResultSet
//import java.util.ArrayList
//import java.util.HashMap
//import java.util.LinkedHashMap
//import kotlin.properties.Delegates
//
//class ResultRow(size: Int, private val fieldIndex: Map<Expression<*>, Int>) {
//    val data = arrayOfNulls<Any?>(size)
//    internal object NotInitializedValue;
//
//
//    /**
//     * Function might returns null. Use @tryGet if you don't sure of nullability (e.g. in left-join cases)
//     */
////    @Suppress("UNCHECKED_CAST")
////    operator fun <T> get(c: Expression<T>) : T {
////        val d:Any? = getRaw(c)
////
////        return d?.let {
////            if (d == NotInitializedValue) error("${c.toSQL(QueryBuilder(false))} is not initialized yet")
////            (c as? ExpressionWithColumnType<T>)?.columnType?.valueFromDB(it) ?: it ?: run {
////                val column = c as? Column<T>
////                if (column?.dbDefaultValue != null && column?.columnType?.nullable == false) {
////                    exposedLogger.warn("Column ${TransactionManager.current().identity(column!!)} is marked as not null, " +
////                            "has default db value, but returns null. Possible have to re-read it from DB.")
////                }
////                null
////            }
////        } as T
////    }
//
//    operator fun <T> set(c: Expression<T>, value: T) {
//        val index = fieldIndex[c] ?: error("${c.toSQL(QueryBuilder(false))} is not in record set")
//        data[index] = value
//    }
//
//    fun<T> hasValue (c: Expression<T>) : Boolean {
//        return fieldIndex[c]?.let{ data[it] != NotInitializedValue } ?: false;
//    }
//}
//
//@Suppress("UNCHECKED_CAST")
//abstract class EntityClass<ID : Any, out T: Entity<ID>>(val table: IdTable<ID>) {
//    open val dependsOnTables: ColumnSet get() = table
//    open val dependsOnColumns: List<Column<out Any?>> get() = dependsOnTables.columns
//}
//
//open class Entity<ID:Any>(val id: EntityID<ID>) {
//    var klass: EntityClass<ID, Entity<ID>> by Delegates.notNull()
//    val writeValues = LinkedHashMap<Column<Any?>, Any?>()
//    var _readValues: ResultRow? = null
//
//    fun storeWrittenValues(storageSession: StorageSession) {
//        // move write values to read values
//        if (_readValues != null) {
//            for ((c, v) in writeValues) {
//                _readValues!!.set(c, v?.let { c.columnType.valueFromDB(storageSession, c.columnType.valueToDB(storageSession, it)!!)})
//            }
//            if (klass.dependsOnColumns.any { it.table == klass.table && !_readValues!!.hasValue(it) } ) {
//                _readValues = null
//            }
//        }
//        // clear write values
//        writeValues.clear()
//    }
//}
//
//class EntityCache {
//    val inserts = HashMap<IdTable<*>, MutableList<Entity<*>>>()
//    fun flushInserts(storageSession: StorageSession, table: IdTable<*>) {
//        inserts.remove(table)?.let {
//            var toFlush: List<Entity<*>> = it
//            do {
//                val partition = toFlush.partition {
//                    it.writeValues.none {
//                        val (key, value) = it
//                        key.referee == table.id && value is EntityID<*> && value._value == null
//                    }
//                }
//                toFlush = partition.first
//                val ids = table.batchInsert(storageSession, toFlush) { entry ->
//                    for ((c, v) in entry.writeValues) {
//                        this.setValue(storageSession,c,v)
//                    }
//                }
//
//                for ((entry, genValues) in toFlush.zip(ids)) {
//                    if (entry.id._value != null) continue
//                    val id = genValues[table.id]!!
//                    entry.id._value = (table.id.columnType as EntityIDColumnType<*>).idColumn.columnType.valueFromDB(storageSession, when (id) {
//                        is EntityID<*> -> id.value(storageSession)
//                        else -> id
//                    })
//                    entry.writeValues[entry.klass.table.id as Column<Any?>] = id
//                    genValues.forEach {
//                        entry.writeValues[it.key as Column<Any?>] = it.value
//                    }
//
//                    entry.storeWrittenValues(storageSession)
////                    TransactionManager.current().entityCache.store<Any,Entity<Any>>(entry)
////                    EntityHook.registerChange(EntityChange(entry.klass as EntityClass<Any, Entity<Any>>, entry.id as EntityID<Any>, EntityChangeType.Created))
//                }
//                toFlush = partition.second
//            } while(toFlush.isNotEmpty())
//        }
//    }
//}
//class StorageSession() {
//    val entityCache = EntityCache()
//    var activeConnection: Connection = null
//}
//
//interface DialectTypeProvider {
//    fun longType(): String
//    fun longAutoIncType(): String
//}
//
//abstract class ExpressionWithColumnType<T> : Expression<T>() {
//    // used for operations with literals
//    abstract val columnType: ColumnType;
//}
//abstract class Op<T>() : Expression<T>() {
//    companion object {
//        inline fun <T> build(op: SqlExpressionBuilder.()-> Op<T>): Op<T> {
//            return SqlExpressionBuilder.op()
//        }
//    }
//}
//
//object SqlExpressionBuilder {
//}
//
//open class FunctionProvider {
//
//    open fun substring(expr: Expression<String?>, start: ExpressionWithColumnType<Int>, length: ExpressionWithColumnType<Int>, builder: QueryBuilder) : String {
//        return "SUBSTRING(${expr.toSQL(builder)}, ${start.toSQL(builder)}, ${length.toSQL(builder)})"
//    }
//
//    open fun random(seed: Int?): String = "RANDOM(${seed?.toString().orEmpty()})"
//
//    open fun cast(expr: Expression<*>, type: ColumnType, builder: QueryBuilder) = "CAST(${expr.toSQL(builder)} AS ${type.sqlType()})"
//
//    open fun<T:String?> ExpressionWithColumnType<T>.match(pattern: String, mode: MatchMode? = null): Op<Boolean> = kotlin.with(SqlExpressionBuilder) { this@match.like(pattern) }
//
//    interface MatchMode {
//        fun mode() : String
//    }
//}
//
//interface Dialect {
//    val name: String
//    val typeProvider: DialectTypeProvider
//    val functionProvider: FunctionProvider
//    fun allTablesNames(): List<String>
//}
//
//internal abstract class VendorDialect(override val name: String,
//                                      val databaseMetaData: DatabaseMetaData,
//                                      override val typeProvider: DialectTypeProvider,
//                                      override val functionProvider: FunctionProvider = FunctionProvider()) : Dialect {
//
//    /* Cached values */
//    private var _allTableNames: List<String>? = null
//    val allTablesNames: List<String>
//        get() {
//            if (_allTableNames == null) {
//                _allTableNames = allTablesNames()
//            }
//            return _allTableNames!!
//        }
//
//    val String.inProperCase: String get() = when {
//        databaseMetaData.storesUpperCaseIdentifiers() -> toUpperCase()
//        databaseMetaData.storesUpperCaseIdentifiers()  -> toLowerCase()
//        else -> this
//    }
//
//    /* Method always re-read data from DB. Using allTablesNames field is preferred way */
//    override fun allTablesNames(): List<String> {
//        val result = ArrayList<String>()
//        val resultSet = databaseMetaData.getTables(null, null, null, arrayOf("TABLE"))
//
//        while (resultSet.next()) {
//            result.add(resultSet.getString("TABLE_NAME").inProperCase)
//        }
//        return result
//    }
//
//    override fun getDatabase(): String = TransactionManager.current().connection.catalog
//
//    override fun tableExists(table: Table) = allTablesNames.any { it == table.nameInDatabaseCase() }
//
//    protected fun ResultSet.extractColumns(tables: Array<out Table>, extract: (ResultSet) -> Triple<String, String, Boolean>): Map<Table, List<Pair<String, Boolean>>> {
//        val mapping = tables.associateBy { it.nameInDatabaseCase() }
//        val result = HashMap<Table, MutableList<Pair<String, Boolean>>>()
//
//        while (next()) {
//            val (tableName, columnName, nullable) = extract(this)
//            mapping[tableName]?.let { t ->
//                result.getOrPut(t) { arrayListOf() } += columnName to nullable
//            }
//        }
//        return result
//    }
//    override fun tableColumns(vararg tables: Table): Map<Table, List<Pair<String, Boolean>>> {
//        val rs = TransactionManager.current().db.metadata.getColumns(getDatabase(), null, null, null)
//        return rs.extractColumns(tables) {
//            Triple(it.getString("TABLE_NAME"), it.getString("COLUMN_NAME")!!, it.getBoolean("NULLABLE"))
//        }
//    }
//
//    private val columnConstraintsCache = HashMap<String, List<ForeignKeyConstraint>>()
//
//    override @Synchronized fun columnConstraints(vararg tables: Table): Map<Pair<String, String>, List<ForeignKeyConstraint>> {
//        val constraints = HashMap<Pair<String, String>, MutableList<ForeignKeyConstraint>>()
//        for (table in tables.map{ it.nameInDatabaseCase() }) {
//            columnConstraintsCache.getOrPut(table, {
//                val rs = TransactionManager.current().db.metadata.getExportedKeys(getDatabase(), null, table)
//                val tableConstraint = arrayListOf<ForeignKeyConstraint> ()
//                while (rs.next()) {
//                    val refereeTableName = rs.getString("FKTABLE_NAME")!!
//                    val refereeColumnName = rs.getString("FKCOLUMN_NAME")!!
//                    val constraintName = rs.getString("FK_NAME")!!
//                    val refTableName = rs.getString("PKTABLE_NAME")!!
//                    val refColumnName = rs.getString("PKCOLUMN_NAME")!!
//                    val constraintDeleteRule = ReferenceOption.resolveRefOptionFromJdbc(rs.getInt("DELETE_RULE"))
//                    tableConstraint.add(ForeignKeyConstraint(constraintName, refereeTableName, refereeColumnName, refTableName, refColumnName, constraintDeleteRule))
//                }
//                tableConstraint
//            }).forEach { it ->
//                constraints.getOrPut(it.refereeTable to it.refereeColumn, {arrayListOf()}).add(it)
//            }
//
//        }
//        return constraints
//    }
//
//    private val existingIndicesCache = HashMap<Table, List<Index>>()
//
//    override @Synchronized fun existingIndices(vararg tables: Table): Map<Table, List<Index>> {
//        for(table in tables) {
//            val tableName = table.nameInDatabaseCase()
//            val metadata = TransactionManager.current().db.metadata
//
//            existingIndicesCache.getOrPut(table, {
//                val pkNames = metadata.getPrimaryKeys(getDatabase(), null, tableName).let { rs ->
//                    val names = arrayListOf<String>()
//                    while(rs.next()) {
//                        rs.getString("PK_NAME")?.let { names += it }
//                    }
//                    names
//                }
//                val rs = metadata.getIndexInfo(getDatabase(), null, tableName, false, false)
//
//                val tmpIndices = hashMapOf<Pair<String, Boolean>, MutableList<String>>()
//
//                while (rs.next()) {
//                    val indexName = rs.getString("INDEX_NAME")!!
//                    val column = rs.getString("COLUMN_NAME")!!
//                    val isUnique = !rs.getBoolean("NON_UNIQUE")
//                    tmpIndices.getOrPut(indexName to isUnique, { arrayListOf() }).add(column)
//                }
//                tmpIndices.filterNot { it.key.first in pkNames }.map { Index(it.key.first, tableName, it.value, it.key.second)}
//            })
//        }
//        return HashMap(existingIndicesCache)
//    }
//
//    override @Synchronized fun resetCaches() {
//        _allTableNames = null
//        columnConstraintsCache.clear()
//        existingIndicesCache.clear()
//    }
//
//    override fun replace(table: Table, data: List<Pair<Column<*>, Any?>>, transaction: Transaction): String {
//        throw UnsupportedOperationException("There's no generic SQL for replace. There must be vendor specific implementation")
//    }
//
//    override fun insert(ignore: Boolean, table: Table, columns: List<Column<*>>, expr: String, transaction: Transaction): String {
//        if (ignore) {
//            throw UnsupportedOperationException("There's no generic SQL for INSERT IGNORE. There must be vendor specific implementation")
//        }
//
//        return "INSERT INTO ${transaction.identity(table)} (${columns.joinToString { transaction.identity(it).inProperCase }}) $expr"
//    }
//
//    override fun delete(ignore: Boolean, table: Table, where: String?, transaction: Transaction): String {
//        if (ignore) {
//            throw UnsupportedOperationException("There's no generic SQL for DELETE IGNORE. There must be vendor specific implementation")
//        }
//
//        return buildString {
//            append("DELETE FROM ")
//            append(transaction.identity(table))
//            if (where != null) {
//                append(" WHERE ")
//                append(where)
//            }
//        }
//    }
//
//    override fun createIndex(unique: Boolean, tableName: String, indexName: String, columns: List<String>): String {
//        val t = TransactionManager.current()
//        return buildString {
//            append("CREATE ")
//            if (unique) append("UNIQUE ")
//            append("INDEX ${t.quoteIfNecessary(indexName)} ON ${t.quoteIfNecessary(tableName)} ")
//            columns.joinTo(this, ", ", "(", ")") {
//                t.quoteIfNecessary(it)
//            }
//        }
//    }
//
//    override fun dropIndex(tableName: String, indexName: String): String {
//        val t = TransactionManager.current()
//        return "ALTER TABLE ${t.quoteIfNecessary(tableName)} DROP CONSTRAINT ${t.quoteIfNecessary(indexName)}"
//    }
//
//    private val supportsSelectForUpdate by lazy { TransactionManager.current().db.metadata.supportsSelectForUpdate() }
//    override fun supportsSelectForUpdate() = supportsSelectForUpdate
//
//    override val supportsMultipleGeneratedKeys: Boolean = true
//
//    override fun limit(size: Int, offset: Int) = "LIMIT $size" + if (offset > 0) " OFFSET $offset" else ""
//
//}
//
//
//class QueryBuilder(val prepared: Boolean) {
//    val args = ArrayList<Pair<ColumnType, Any?>>()
//
//    fun <T> registerArgument(sqlType: ColumnType, argument: T) = registerArguments(sqlType, listOf(argument)).single()
//
//    fun <T> registerArguments(sqlType: ColumnType, arguments: Iterable<T>): List<String> {
//        val argumentsAndStrings = arguments.map { it to sqlType.valueToString(it) }.sortedBy { it.second }
//
//        return argumentsAndStrings.map {
//            if (prepared) {
//                args.add(sqlType to it.first)
//                "?"
//            } else {
//                it.second
//            }
//        }
//    }
//}
//
//abstract class Expression<out T>() {
//    private val _hashCode by lazy {
//        toString().hashCode()
//    }
//
//    abstract fun toSQL(queryBuilder: QueryBuilder): String
//
//    override fun equals(other: Any?): Boolean {
//        return (other as? Expression<*>)?.toString() == toString()
//    }
//
//    override fun hashCode(): Int {
//        return _hashCode
//    }
//
////
////    override fun toString(): String {
////        return toSQL(QueryBuilder(false))
////    }
////
////    companion object {
////        inline fun <T> build(builder: SqlExpressionBuilder.()->Expression<T>): Expression<T> {
////            return SqlExpressionBuilder.builder()
////        }
////    }
//}
//
//abstract class ColumnType(var nullable: Boolean = false, var autoinc: Boolean = false) {
//    abstract fun sqlType(dialect: Dialect): String
//    open fun valueFromDB(storageSession: StorageSession, value: Any): Any  = value
//    fun valueToDB(storageSession: StorageSession, value: Any?): Any? = if (value != null) notNullValueToDB(value) else null
//    open fun notNullValueToDB(value: Any): Any  = value
//    fun valueToString(value: Any?) : String {
//        return when (value) {
//            null -> {
//                if (!nullable) error("NULL in non-nullable column")
//                "NULL"
//            }
//
//            DefaultValueMarker -> "DEFAULT"
//
//            is Iterable<*> -> {
//                value.joinToString(","){ valueToString(it) }
//            }
//
//            else ->  {
//                nonNullValueToString (value)
//            }
//        }
//    }
//
//    protected open fun nonNullValueToString(value: Any) : String {
//        return notNullValueToDB(value).toString()
//    }
//}
//
//
//class EntityIDColumnType<T:Any>(val idColumn: Column<T>, autoinc: Boolean = idColumn.columnType.autoinc): ColumnType(autoinc = autoinc) {
//    override fun sqlType(dialect: Dialect): String = idColumn.columnType.sqlType(dialect)
//
////    override fun notNullValueToDB(value: Any): Any {
////        return idColumn.columnType.notNullValueToDB(when (value) {
////            is EntityID<*> -> value.value
////            else -> value
////        })
////    }
//
//    override fun valueFromDB(storageSession: StorageSession, value: Any): Any {
//        @Suppress("UNCHECKED_CAST")
//        return when (value) {
//            is EntityID<*> -> EntityID(value.value(storageSession) as T, idColumn.table as IdTable<T>)
//            else -> EntityID(idColumn.columnType.valueFromDB(storageSession, value) as T, idColumn.table as IdTable<T>)
//        }
//    }
//}
//
//enum class StatementGroup {
//    DDL, DML
//}
//
//enum class StatementType(val group: StatementGroup) {
//    INSERT(StatementGroup.DML), UPDATE(StatementGroup.DML), DELETE(StatementGroup.DML), SELECT(StatementGroup.DML),
//    CREATE(StatementGroup.DDL), ALTER(StatementGroup.DDL), TRUNCATE(StatementGroup.DDL), DROP(StatementGroup.DDL)
//}
//
//abstract class Statement<out T>(val type: StatementType, val targets: List<Table>) {
//    fun execute(storageSession: StorageSession): T? {
//        return null
//    }
//}
//
//internal object DefaultValueMarker {
//    override fun toString(): String = "DEFAULT"
//}
//
//class BatchInsertStatement(val table: Table, val _ignore: Boolean = false): Statement<List<Map<Column<*>, Any>>>(StatementType.INSERT, listOf(table)) {
//    val data = ArrayList<LinkedHashMap<Column<*>, Any?>>()
//
//    fun addBatch() {
//        data.add(LinkedHashMap())
//        arguments = null
//    }
//
//    fun <T> setValue(storageSession: StorageSession, column: Column<T>, value: T) {
//        val values = data.last()
//
//        if (values.containsKey(column)) {
//            error("$column is already initialized")
//        }
//
//        values.put(column, column.columnType.valueToDB(storageSession, value))
//    }
//
//    private var arguments: List<List<Pair<Column<*>, Any?>>>? = null
//        get() {
//            return field ?: listOf(data.flatMap { single ->
//                table.columns.filterNot { it.columnType.autoinc }.map {
//                    it to (single[it] ?: it.defaultValueFun?.invoke() ?: it.dbDefaultValue?.let { DefaultValueMarker })
//                }
//            }).apply { field = this }
//        }
//}
//interface FieldSet {
//    val fields: List<Expression<*>>
//    val source: ColumnSet
//}
//
//abstract class ColumnSet(): FieldSet {
//    abstract val columns: List<Column<*>>
//    override val fields: List<Expression<*>> get() = columns
//    override val source = this
//}
//
//open class Table(name: String=""): ColumnSet() {
//    open val tableName = (if (name.isNotEmpty()) name else this.javaClass.simpleName.removeSuffix("Table"))
//    private val _columns = ArrayList<Column<*>>()
//    override val columns: List<Column<*>> = _columns
//
//    fun <T> registerColumn(name: String, type: ColumnType): Column<T> = Column<T>(this, name, type).apply {
//        _columns.add(this)
//    }
//
//    fun <T> Column<T>.primaryKey(indx: Int? = null): Column<T> {
//        if (indx != null && table.columns.any { it.indexInPK == indx } ) throw IllegalArgumentException("Table $tableName already contains PK at $indx")
//        indexInPK = indx ?: table.columns.count { it.indexInPK != null } + 1
//        return this
//    }
//
//    fun<TColumn: Column<*>> replaceColumn (oldColumn: Column<*>, newColumn: TColumn) : TColumn {
//        _columns.remove(oldColumn)
//        _columns.add(newColumn)
//        return newColumn
//    }
//
//    @Suppress("UNCHECKED_CAST")
//    fun <T:Any> Column<T>.entityId(): Column<EntityID<T>> = replaceColumn(this, Column<EntityID<T>>(table, name, EntityIDColumnType(this)).apply {
//        this.indexInPK = this@entityId.indexInPK
//        this.defaultValueFun = this@entityId.defaultValueFun?.let { { EntityID(it(), table as IdTable<T>) } }
//    })
//
//    fun <E:Any> batchInsert(storageSession: StorageSession, data: Iterable<E>, ignore: Boolean = false, body: BatchInsertStatement.(E)->Unit): List<Map<Column<*>, Any>> {
//        if (data.count() == 0) return emptyList()
//        BatchInsertStatement(this, ignore).let {
//            for (element in data) {
//                it.addBatch()
//                it.body(element)
//            }
//            return it.execute(storageSession)!!
//        }
//    }
//    fun long(name: String): Column<Long> = registerColumn(name, LongColumnType())
//}
//
//class LongColumnType(autoinc: Boolean = false): ColumnType(autoinc = autoinc) {
//    override fun sqlType(dialect: Dialect): String  = dialect.typeProvider.let { if (autoinc) it.longAutoIncType() else it.longType() }
//
//    override fun valueFromDB(storageSession: StorageSession, value: Any): Any {
//        return when(value) {
//            is Long -> value
//            is Number -> value.toLong()
//            else -> error("Unexpected value of type Long: $value")
//        }
//    }
//}
//
//
//
//open class Column<T: Any?>(val table: Table, val name: String, val columnType: ColumnType): Expression<T>() {
//    var referee: Column<*>? = null
//    internal var indexInPK: Int? = null
//    internal var defaultValueFun: (() -> T)? = null
//    internal var dbDefaultValue: Expression<T>? = null
//
//    override fun toSQL(queryBuilder: QueryBuilder): String = "${quoteIfNecessary(this.table.tableName.inProperCase())}.${quoteIfNecessary(this.name.inProperCase())}"
//}
//
//fun <N:Number, C:Column<N>> C.autoIncrement(): C {
//    columnType.autoinc = true
//    return this
//}
//
//abstract class IdTable<T:Any>(name: String): Table(name) {
//    abstract val id : Column<EntityID<T>>
//}
//
//open class LongIdTable(name: String = "", columnName: String = "id") : IdTable<Long>(name) {
//    override val id: Column<EntityID<Long>> = long(columnName).autoIncrement().primaryKey().entityId()
//}
//
//class EntityID<T:Any>(id: T?, val table: IdTable<T>) {
//    var _value: Any? = id
//
//    fun value(storageSession: StorageSession): T {
//        if (_value == null) {
//            storageSession.entityCache.flushInserts(storageSession, table)
//            assert(_value != null) { "Entity must be inserted" }
//        }
//
//        @Suppress("UNCHECKED_CAST")
//        return _value!! as T
//    }
//}
