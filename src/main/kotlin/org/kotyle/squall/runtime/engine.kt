package org.kotyle.squall.runtime.engine

import org.kotyle.squall.runtime.session.Session
import javax.sql.DataSource

class Engine(val dataSource: DataSource) {
    fun  createSession() = Session(dataSource.connection)

}