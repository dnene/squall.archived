package org.kotyle.squall.runtime.session

import org.kotyle.squall.schema.tables.Column
import org.kotyle.squall.schema.tables.ConstrainedColumnType
import org.kotyle.squall.schema.tables.Table
import org.kotyle.squall.schema.tables.commands.*
import org.kotyle.squall.util.SquallRuntimeException
import org.kotyle.squall.util.SquallSchemaException
import org.kotyle.squall.util.executeX
import org.slf4j.LoggerFactory
import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.Statement
import java.util.function.Consumer

val log = LoggerFactory.getLogger("org.kotyle.squall.runtime.session")
data class SqlStatementsForTable(val insertPrepared: String)

class ResultSetIterator(val rs: ResultSet): Iterator<Array<Any?>> {
    val columnCount = rs.getMetaData().columnCount
    var record: Array<Any?>? = null
    var navigated = false
    var lastResult: Boolean = true
    override fun next(): Array<Any?> {
        if (if (navigated) lastResult else navigate()) {
            navigated = false
            lastResult = true
            return (1..columnCount).map {
                rs.getObject(it)
            }.toTypedArray()
        } else {
            throw SquallRuntimeException("err-scan-past-end-of-resultset")
        }

    }

    override fun forEachRemaining(action: Consumer<in Array<Any?>>) {
        super.forEachRemaining(action)
    }

    fun navigate(): Boolean =
        rs.next().apply {
            lastResult = this
            navigated = true
        }

    override fun hasNext(): Boolean = if (navigated) lastResult else navigate()
}

class ResultSetToSequence(val rs: ResultSet): Sequence<Array<Any?>> {
    override fun iterator(): Iterator<Array<Any?>> = ResultSetIterator(rs)
}

fun ResultSet.toSequence(): Sequence<Array<Any?>> = ResultSetToSequence(this)
fun ResultSet.toIterator(): Iterator<Array<Any?>> = ResultSetIterator(this)


interface DbDialect {
    val dialectName: String
//    val statementStringCache:Map <Table, SqlStatementsForTable>
    fun generateColumnSpecificationFor(column: Column<*>): String
    fun generateTableNameFor(table: Table): String
    fun generateColumnNameFor(column: Column<*>): String
    fun getSqlStatementsForTable(table: Table): SqlStatementsForTable
}

object MySQLDialect: DbDialect {
    val statementStringCache = mutableMapOf<Table, SqlStatementsForTable>()
    override val dialectName: String = "MySQL"

    private fun getSqlStatementsForTableInner(table: Table) : SqlStatementsForTable {
        val preparedInsertStatement = "INSERT INTO ${generateTableNameFor(table)} " +
                "(${table.columns.map { generateColumnNameFor(it) }.joinToString(", ")}) VALUES (" +
                "${table.columns.map { "?" }.joinToString(", ")})"
        return SqlStatementsForTable(preparedInsertStatement).apply { statementStringCache[table] = this }
    }
    override fun getSqlStatementsForTable(table: Table): SqlStatementsForTable =
        statementStringCache.get(table) ?: getSqlStatementsForTableInner(table)


    //todo: introduce underscores based on camel case
    override fun generateTableNameFor(table: Table) = table._name.toLowerCase()

    override fun generateColumnSpecificationFor(column: Column<*>): String {
        val columnType = column.columnType
        val colname = when(columnType.javaSqlType) {
            java.sql.Types.BIGINT -> "BIGINT"
            java.sql.Types.INTEGER -> "INT"
            java.sql.Types.VARCHAR -> "VARCHAR(${(columnType as ConstrainedColumnType).maxlen})"
            else -> throw SquallSchemaException("err-invalid-column-type",
                    mapOf("column-java-sql-type" to columnType.javaSqlType,
                            "column-name" to column.name,
                            "table-name" to column.table._name,
                            "dialect-name" to dialectName,
                            "column-type-class" to columnType.javaClass))
        }
        return listOf<String?>(colname,
                if (columnType.nullable) null else "NOT NULL",
                // todo: What if there are more primary keys
                if (columnType.autoinc) "AUTO_INCREMENT PRIMARY KEY" else null).filterNotNull().joinToString (" ")
    }

    override fun generateColumnNameFor(column: Column<*>): String = column.name.toLowerCase()
}

class StatementGenerator(val connection: Connection, val dbDialect: DbDialect) {
    fun generateCreateStatementFor(table: Table): String {
        val columnListStr = table.columns.map { it.name + " " + dbDialect.generateColumnSpecificationFor(it) }.joinToString(", \n\t")
        val createString = "CREATE TABLE ${dbDialect.generateTableNameFor(table)} (\n\t${columnListStr}\n);"
        return createString
    }

    fun generateDropTableStatementFor(table: Table): String = "DROP TABLE ${dbDialect.generateTableNameFor(table)};"
    fun generateSelectRowsStatementFor(table: Table): String {
        val sql = "SELECT ${table.columns.map { dbDialect.generateColumnNameFor(it)}.joinToString(", ")} FROM ${dbDialect.generateTableNameFor(table)}"
        return sql
    }

    fun generateUpdateRowsStatementFor(table: Table): String {
        val sql = "UPDATE ${dbDialect.generateTableNameFor(table)}"
        return sql
    }

    fun generateDeleteRowsStatementFor(table: Table): String {
        val sql = "DELETE FROM ${dbDialect.generateTableNameFor(table)}"
        return sql
    }

    fun generateInsertPreparedStatementFor(table: Table): String = dbDialect.getSqlStatementsForTable(table).insertPrepared
    fun getNewPreparedStatement(table: Table, statementType: StatementType): PreparedStatement {
        val sql = when (statementType) {
            StatementType.SELECT -> generateSelectRowsStatementFor(table)
            StatementType.INSERT -> generateInsertPreparedStatementFor(table)
            StatementType.UPDATE -> generateUpdateRowsStatementFor(table)
            StatementType.DELETE -> generateDeleteRowsStatementFor(table)
        }
        log.debug("New prepared statement generated for table: ${table._name}")
        return when {
            statementType.returnsIds -> connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)
            else -> connection.prepareStatement(sql)
        }
    }
}

enum class StatementType(val immediate: Boolean, val returnsIds: Boolean) {
    SELECT(true, false),
    INSERT(false, true),
    UPDATE(true, false),
    DELETE(true, false)
}

class Session(val connection: Connection) {
    val statementGenerator = StatementGenerator(connection, MySQLDialect)
    val preparedStatementCache = mutableMapOf<Pair<Table, StatementType>, PreparedStatement>()
    val statements = mutableListOf<SqlStatement>()
    val metadata = connection.metaData
    fun  createTables(vararg tables: Table) {
        tables.forEach {
            statementGenerator.generateCreateStatementFor(it).let { sql -> connection.createStatement().use { it.executeX(sql) }}
        }
    }

    fun getPreparedStatement(table: Table, statementType: StatementType): PreparedStatement =
        preparedStatementCache.get(Pair(table, statementType)) ?:
                (statementGenerator.getNewPreparedStatement(table, statementType).apply {
                    preparedStatementCache[Pair(table, statementType)] = this
                })

    fun dropTables(vararg tables: Table) {
        tables.forEach {
            statementGenerator.generateDropTableStatementFor(it).let { sql -> connection.createStatement().use { it.executeX(sql) }}
        }
    }

    fun <T: Table> select(table: T): RecordData<T> {
        SelectStatement<T>(table).apply { statements.add(this) }
        flush()
        return RecordData<T>(table)
    }

    fun <T: Table> insert(table: T, initialiser: RecordData<T>.() -> Unit): RecordData<T> =
            InsertStatement<T>(table).apply { initialiser()}.apply { statements.add(this) }

    fun <T: Table> insert(vararg insert: InsertStatement<T>) =
            insert.forEach { statements.add(it) }

    fun <T: Table, R: Number> insertID(table: T, initialiser: RecordData<T>.() -> Unit): RefID<T,R> =
            InsertIDStatement<T,R>(table).apply { initialiser()}.apply { statements.add(this) }.generatedID

    fun <T: Table, R: Number> insertID(vararg insert: InsertIDStatement<T,R>): List<RefID<T,R>> =
            insert.map { statements.add(it); it.generatedID }

    fun <T: Table> delete(table: T) { statements.add(DeleteStatement<T>(table)) }

    fun flush() {
        // todo: error checking
        val groupedStatements = statements.groupBy { Pair(it.table, it.statementType) }
        log.debug("Number of statements: ${statements.size}: ${preparedStatementCache.size}")
        groupedStatements.entries.forEach { (k, statements) ->
            log.debug("Running statements of type ${k.second} on table ${k.first._name}")
            statements.forEach {
                it.execute(this)
            }
            preparedStatementCache.get(k)?.let { ps ->
                val result = ps.executeBatch()
                log.debug("Execute Batch result is ${result}")
                if (k.second.returnsIds) {
                    val generatedIds = ps.generatedKeys.toSequence().toList().flatMap { it.toList() }
                    log.debug("Generated ids are: ${generatedIds.joinToString(",")}")
                    if (statements.size == generatedIds.size) {
                        statements.zip(generatedIds).forEach { (statement, id) ->
                            (statement as? InsertIDStatement<*,*>)?.let { stmt ->
                                id?.let { stmt.generatedID.setIdentifer(id) }
                            }
                        }
                    } else throw SquallRuntimeException("err-generated-ids-mismatch")
                }
            }
        }
        statements.clear()
    }

    fun commit() {
        flush()
        log.debug("Committing transaction")
        connection.commit()
        preparedStatementCache.clear()
    }

}