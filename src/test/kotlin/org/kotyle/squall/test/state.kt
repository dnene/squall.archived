package org.kotyle.squall.test

import com.zaxxer.hikari.HikariDataSource
import org.junit.Test
import org.kotyle.squall.runtime.engine.Engine
import org.kotyle.squall.schema.tables.ID
import org.kotyle.squall.schema.tables.LongIdVersionableTable
import org.kotyle.squall.schema.tables.commands.InsertIDStatement
import org.kotyle.squall.schema.tables.commands.InsertStatement
import org.kotyle.squall.test.StatesTable.code
import org.kotyle.squall.test.StatesTable.description
import org.kotyle.squall.test.StatesTable.id
import org.kotyle.squall.test.StatesTable.name
import org.kotyle.squall.test.StatesTable.version
import org.slf4j.LoggerFactory
import javax.sql.DataSource

val log = LoggerFactory.getLogger("org.kotyle.squall.test")
object StatesTable: LongIdVersionableTable() {
    val code = varchar("code", 2)
    val name = varchar("name", 30)
    val description = varchar("description", 128, nullable = true)
}

open class TestSquall {
    open fun dataSource(): DataSource {
        val ds = HikariDataSource();
        ds.jdbcUrl = "jdbc:mysql://localhost/hatboxn?useSSL=false"
        ds.username = "hatboxn"
        ds.password = "hatboxn"
        ds.isAutoCommit = false
        ds.addDataSourceProperty("databaseName", "hatboxn")
//        ds.driverClassName = "com.mysql.jdbc.Driver"
        ds.dataSourceClassName = "com.mysql.jdbc.jdbc2.optional.MysqlDataSource"
        return ds
    }

}

class TestState: TestSquall() {
//    @Test
    fun testCreateSchema() {
        val engine = Engine(dataSource())
        val session = engine.createSession()
        println(session.connection)
        session.dropTables(StatesTable)
        session.createTables(StatesTable)
    }

//    @Test
    fun testDeleteAll() {
        val engine = Engine(dataSource())
        val session = engine.createSession()
        session.delete(StatesTable)
        session.commit()
    }

    @Test
    fun testInsert() {
        val engine = Engine(dataSource())
        val session = engine.createSession()
        session.delete(StatesTable)
        session.commit()
        log.debug("XXXXX ${StatesTable.id}")
        val jkid = session.insertID<StatesTable,Long>(StatesTable) {
            this[id] = ID(1L)
            this[code] = "JK"
            this[name] = "Jammu & Kashmir"
            this[description] = "Northern State"
        }
        val otherIDs = session.insertID(
                InsertIDStatement<StatesTable,Long>(StatesTable) {
                    this[code] = "MH"
                    this[name] = "Maharashtra"
                    this[description] = "Largest Industrial State in Western India"
                },
                InsertIDStatement<StatesTable,Long>(StatesTable) {
                    this[code] = "GJ"
                    this[name] = "Gujarat"
                    this[description] = "Westernmost state"
                })
        session.commit()

        log.debug("JK ID Is: ${jkid.id}")
        log.debug("Other IDs are: ${otherIDs.map { it.id }}")

        val data = session.select(StatesTable)
        log.debug("Done")
    }
}